import GameQueue.MyQueue;

/**
 * @author Christopher J. Bigelow, A01771452
 * Developed for Assignment 1 of CS 3 Spring 2019 at Utah State University.
 */
public class Application {
    /*Keep track of whether or not puzzle was solved.*/
    private boolean solved = false;

    /*Single MyQueue instance is used.*/
    private MyQueue<GameMove> gameQueue = new MyQueue<>();

    /**
     * Puzzle Solver. Takes board as parameter and returns Statistic instance. If after the 10,000,000 attempt it can't
     * solve the puzzle, it stops and returns a Statistic package with an invalid pattern.
     *
     * @param gameBoard to be solved
     * @return Statistic instance with runtime stats and pattern.
     */
    public Statistic solver(Board gameBoard) {

        /*Arbitrarily stop solving after 10,000,000 move tries*/
        int limit = 10000000;

        /*Init variable to hold pattern*/
        String playsToWin = "";

        /*Move Counter*/
        int count = 0;

        /*runtime Statistic instance init*/
        Statistic runStats = new Statistic();

        /*Start stopwatch*/
        runStats.start();

        /*Adds first four moves to queue*/
        this.addMoreMoves(gameBoard.duplicate(), "", runStats);

        /*While puzzle is not solved, move count is under arbitrary limit, and our queue has items*/
        while (!solved && count < limit && gameQueue.hasItems()) {

            /*Increase move counter*/
            count += 1;

            /*Pull first move from the queue*/
            GameMove currentMove = gameQueue.dequeue();

            /*Increase attempt counter*/
            runStats.incrementTried();

            /*Check if puzzle matches goal board version*/
            if (!currentMove.isFinished()) {

                /*If not make purposed move*/
                if (currentMove.makeMove()) {

                    /*Then add more moves to the queue if successful.*/
                    this.addMoreMoves(currentMove.currentBoard.duplicate(), currentMove.previousMoves, runStats);
                }
            } else {

                /*If it is set solved to true*/
                solved = true;

                /*Obtain the pattern to win*/
                playsToWin = currentMove.getPreviousMoves();

                /*Clear queue*/
                gameQueue.clearQueue();
            }
        }

        /*Stop watch after while loop terminates*/
        runStats.end();

        /*Verify board was solved under our predefined limit
         * Add pattern to Statistic package depending on answer*/
        if (count < limit) {
            System.out.printf("The winning formula: %s\n", playsToWin);
            runStats.setPattern(playsToWin);
        } else {
            System.out.println("Board is Unsolvable");
            runStats.setPattern("XXX");
        }

        /*Return statistic instance*/
        return runStats;
    }

    /**
     * Add moves to queue. Designed so as to not add a move that would reverse the most recently played move.
     * Though other logic exists elsewhere in the program to prevent this, this saves us time in out while loop in the
     * solver method. Duplicates board before queue insertion.
     * @param aBoard to make the move on.
     * @param previousMoves to track the moves we have already made.
     * @param statPackage Statistic instance for attempt and queue tracking.
     */
    public void addMoreMoves(Board aBoard, String previousMoves, Statistic statPackage) {
        GameMove moveLeft = new GameMove(aBoard.duplicate(), 'L', previousMoves);
        GameMove moveRight = new GameMove(aBoard.duplicate(), 'R', previousMoves);
        GameMove moveUp = new GameMove(aBoard.duplicate(), 'U', previousMoves);
        GameMove moveDown = new GameMove(aBoard.duplicate(), 'D', previousMoves);
        if (previousMoves.length() > 0) {
            /* Switch statement based of the previous move. Prevents reversal of last move.
             */
            switch (previousMoves.charAt(previousMoves.length() - 1)) {
                case 'L':
                    gameQueue.enqueue(moveLeft);
                    gameQueue.enqueue(moveUp);
                    gameQueue.enqueue(moveDown);
                    break;
                case 'R':
                    gameQueue.enqueue(moveRight);
                    gameQueue.enqueue(moveUp);
                    gameQueue.enqueue(moveDown);
                    break;

                case 'U':
                    gameQueue.enqueue(moveLeft);
                    gameQueue.enqueue(moveRight);
                    gameQueue.enqueue(moveUp);
                    break;
                case 'D':
                    gameQueue.enqueue(moveLeft);
                    gameQueue.enqueue(moveRight);
                    gameQueue.enqueue(moveDown);
                    break;
            }
            statPackage.incrementQueue(3);
        } else {
            /* Else is just ran at beginning if done successfully.*/
            gameQueue.enqueue(moveLeft);
            gameQueue.enqueue(moveRight);
            gameQueue.enqueue(moveUp);
            gameQueue.enqueue(moveDown);
            statPackage.incrementQueue(4);
        }


    }

    /*
     * Private move class.
     */
    private static class GameMove {
        Board currentBoard;
        char moveToBeMade;
        char lastMove;
        private String previousMoves;

        /**
         * Move Constructor. Takes board, move and previous moves. Calculates what the last move was.
         * @param b board to have move made on.
         * @param move to be made.
         * @param prevMoves list to prevent reversal of last move.
         */
        public GameMove(Board b, char move, String prevMoves) {
            this.currentBoard = b;
            this.moveToBeMade = move;
            this.previousMoves = prevMoves;
            if (this.previousMoves.length() == 0) {
                this.lastMove = 'N';
            } else {
                this.lastMove = this.previousMoves.substring(this.previousMoves.length() - 1).charAt(0);
            }
        }

        /**
         * Accesses moveToBeMade and makes move on board. Adds move that was made to previously made move.
         * @return true if successful.
         */
        public boolean makeMove() {
            if (this.moveToBeMade == this.currentBoard.makeMove(this.moveToBeMade, this.lastMove)) {
                this.previousMoves = this.previousMoves + this.moveToBeMade;
                return true;
            }
            return false;
        }

        /**
         * Accesses previously made moves
         * @return this.previousMoves.
         */
        public String getPreviousMoves() {
            return this.previousMoves;
        }

        /**
         * Checks if board matches finished state board.
         * @return true if board is complete
         */
        public boolean isFinished() {
            Board goalBoard = new Board();
            int[] values = {
                    1, 2, 3,
                    4, 5, 6,
                    7, 8, 0
            };
            goalBoard.makeBoard(values);
            return goalBoard.equals(this.currentBoard);
        }


    }
}