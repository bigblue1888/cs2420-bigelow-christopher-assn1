import java.util.Scanner;

/**
 * Code provided by professor. Modifications made as noted.
 */
public class Game {

    /**
     * For each puzzle the board is ran through the added playTheGame method.
     *
     * @param args
     */
    public static void main(String[] args) {
        Game g = new Game();
        Scanner in = new Scanner(System.in);

        System.out.println("Starting new Game");
        int[] game0 = {1, 2, 3, 7, 4, 0, 6, 5, 8};
        Board b = new Board();
        b.makeBoard(game0);
        g.playGiven("game 0", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        String resp;
        resp = in.nextLine();

        System.out.println("Starting new Game");

        int[] game1 = {1, 3, 2, 4, 5, 6, 8, 7, 0};
        b.makeBoard(game1);
        g.playGiven("game 1", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        resp = in.nextLine();

        System.out.println("Starting new Game");
        int[] game2 = {1, 3, 8, 6, 2, 0, 5, 4, 7};
        b.makeBoard(game2);
        g.playGiven("game 2", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        resp = in.nextLine();

        System.out.println("Starting new Game");
        int[] game3 = {4, 0, 1, 3, 5, 2, 6, 8, 7};
        b.makeBoard(game3);
        g.playGiven("game 3", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        resp = in.nextLine();

        System.out.println("Starting new Game");
        int[] game4 = {7, 6, 4, 0, 8, 1, 2, 3, 5};  // Warning slow to solve
        b.makeBoard(game4);
        g.playGiven("game 4", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        resp = in.nextLine();

        System.out.println("Starting new Game");
        int[] game5 = {1, 2, 3, 4, 5, 6, 8, 7, 0};   // Warning unsolvable
        b.makeBoard(game5);
        g.playGiven("game 5", b);
        playTheGame(b);
        System.out.println("Click any key to continue\n");
        resp = in.nextLine();

        boolean playAgain = true;

        int JUMBLECT = 4;  // how much jumbling to to in random board
        while (playAgain) {
            System.out.println("Starting new Game");
            b = g.playRandom("Random Board", JUMBLECT);
            playTheGame(b);
            System.out.println("Play Again?  Answer Y for yes\n");
            resp = in.nextLine().toUpperCase();
            playAgain = (resp.charAt(0) == 'Y');
        }


    }

    /**
     * Bulk of modification occurs here. This method take the board and runs it through an Application instance using
     * the solver method. Handles all the printing of stats and patterns.
     * @param b the Board to be played.
     */
    private static void playTheGame(Board b) {
        Application gameApp = new Application();
        Statistic runGame;
        runGame = gameApp.solver(b);
        runGame.report();

        /*Check if board was solved correctly. If so run the implemented showMe method*/
        if (!runGame.pattern.contains("XXX")) {
            b.showMe(b, runGame.pattern);
        }


    }

    public void playGiven(String label, Board b) {
        System.out.println(label + "\n" + b);

    }

    public Board playRandom(String label, int jumbleCount) {
        Board b = new Board();
        b.makeBoard(jumbleCount);
        System.out.println(label + "\n" + b);
        return b;
    }


}
