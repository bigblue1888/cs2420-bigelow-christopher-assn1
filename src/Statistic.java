/**
 * @author Christopher J. Bigelow, A01771452
 * Developed for Assignment 1 of CS 3 Spring 2019 at Utah State University.
 */
public class Statistic {
    int itemsQueued = 0;
    int itemsTried = 0;
    long startTime;
    long endTime;
    long totalTime;
    String pattern;

    /**
     * Start time clock
     */
    public void start() {
        this.startTime = System.currentTimeMillis();
    }

    /**
     * Stop time clock and calculate total time
     */
    public void end() {
        this.endTime = System.currentTimeMillis();
        this.totalTime = (endTime - startTime);
    }

    /**
     * Stores winning pattern to Instance
     *
     * @param pat the pattern to be stored.
     */
    public void setPattern(String pat) {
        this.pattern = pat;
    }

    /**
     * Increment itemsQueued by 1.
     */
    public void incrementQueue() {
        this.itemsQueued += 1;
    }

    /**
     * Increment itemsQueued by inc amount.
     * @param inc the amount to be incremented by.
     */
    public void incrementQueue(int inc) {
        this.itemsQueued += inc;
    }

    /**
     * Increment itemsTried by one.
     */
    public void incrementTried() {
        this.itemsTried += 1;
    }

    /**
     * Report total runtime of clock
     */
    public void printRuntime() {
        System.out.printf("Solve time: %d ms\n", this.totalTime);
    }

    /**
     * Report total items queued.
     */
    public void printItemsQueued(){
        System.out.printf("Total Items Queued: %d\n", this.itemsQueued);
    }

    /**
     * Report total items tried.
     */
    public void printItemsTried(){
        System.out.printf("Total Items Tried: %d\n", this.itemsTried);
    }

    /**
     * Provides total report.
     */
    public void report() {
        this.printRuntime();
        this.printItemsQueued();
        this.printItemsTried();
    }
}
