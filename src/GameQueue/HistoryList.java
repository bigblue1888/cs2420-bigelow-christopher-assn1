package GameQueue;

import java.util.Collection;
import java.util.ListIterator;

public interface HistoryList<P> extends Collection<P> {

    boolean add(int position, P play);

    boolean add(P play);

    boolean addAll(P[] plays);

    boolean addFirst(P play);

    boolean addLast(P play);

    void clear();

    P get(int position);

    P getFirst();

    P getLast();

    int indexOf(Object play);

    int lastIndexOf(P play);

    ListIterator listIterator(int index);

    P remove(int position);

    default boolean remove(Object play){
        int before = size();
        int position = indexOf(play);
        remove(position);
        return before > size();
    }

    P set(int position, P player);

    @Override
    default boolean isEmpty() {
        return size() == 0;
    }



}
