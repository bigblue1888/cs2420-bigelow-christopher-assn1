package GameQueue;

public class MyQueue<E> {
    private MyPlayHistory<E> list = new MyPlayHistory<>();

    /**
     * Basic Enqueue Function. Added item to back of the list.
     *
     * @param item to be added
     * @return True if added successfully.
     */
    public boolean enqueue(E item) {
        return list.add(item);
    }

    /**
     * Basic Dequeue Function. Removes first item from the list.
     * @return item removed from list
     */
    public E dequeue() {
        return list.removeFirst();
    }

    /**
     * Checks if queue is empty
     * @return True if list is not empty
     */
    public boolean hasItems() {
        return !list.isEmpty();
    }

    /**
     * Removes All items From Queue
     */
    public void clearQueue() {
        list.clear();
    }

    /**
     * Query the queue for it's size
     * @return Size of queue
     */
    public int size() {
        return list.size();
    }

    /**
     * Pulls items from one queue to add to another.
     * @param q MyQueue class of queue
     */
    public void addItems(MyQueue<? extends E> q) {
        while (q.hasItems()) {
            list.addLast(q.dequeue());
        }
    }
}
