package GameQueue;

import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.zip.DataFormatException;

/**
 * Double Linked List Implementation Coded For Assignment 1 of CS 3 Spring 2019.
 *
 * @param <P> Type of elements that this list contains
 * @author Christopher J. Bigelow, A01771452
 */
public class MyPlayHistory<P> implements HistoryList<P> {
    private Node<P> first, last;
    private int queueSize = 0;

    /**
     * Constructor with no arguments
     **/
    public MyPlayHistory() {
    }

    /**
     * Constructor with one play argument
     **/
    public MyPlayHistory(P play) {
        add(play);
    }

    /**
     * Constructor to take a whole array of plays
     **/
    public MyPlayHistory(P[] plays) {
        for (P play : plays) {
            add(play);
        }
    }

    /**
     * Method to check the size of the list or queue
     *
     * @return int of class attribute queueSize.
     **/
    final public int size() {
        return this.queueSize;
    }

    /**
     * Method to increase queue size
     *
     * @param increment amount. Positive integer.
     **/
    private void increaseQueueSize(int increment) throws DataFormatException {
        if (increment > 0) {
            this.queueSize += increment;
        } else {
            throw new DataFormatException("Expected a positive integer");
        }
    }

    /**
     * Method to decrease queue size
     *
     * @param decrement amount. Positive integer.
     **/
    private void decreaseQueueSize(int decrement) throws DataFormatException {
        if (decrement > 0) {
            this.queueSize -= decrement;
        } else {
            throw new DataFormatException("Decrement Must by a positive integer");
        }
    }

    /**
     * Method to add single play to list. As position is not noted, calls {@link #addLast} method.
     *
     * @param play to be added
     * @return boolean whether or not play was added successfully.
     */
    final public boolean add(P play) {
        return addLast(play);
    }

    /**
     * Method to add array of plays to list. Calls {@link #add(P)} for each item in array.
     *
     * @param plays an array of type P[]
     * @return boolean as to weather of not plays were added successfully.
     */
    @Override
    final public boolean addAll(P[] plays) {
        for (P play : plays) {

            boolean a = this.add(play);
            if (!a) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to add at a particular position or index.
     *
     * @param position index at which to insert new Node
     * @param element  information to be added to new Element.
     * @return true if play was added correctly.
     */
    @Override
    final public boolean add(int position, P element) {
        isValidPosition(position);

        if (position == this.queueSize) {
            return addLast(element);
        } else {
            return addBefore(element, getNode(position));
        }
    }

    /**
     * Validates an integer as an index or position
     *
     * @param position or index being tested
     * @return true if position or index is within the array
     */
    private boolean isValidPosition(int position) {
        if (position >= 0 && position <= this.queueSize) {
            return true;
        } else {
            throw new IndexOutOfBoundsException(String.format("Index %d is out of bounds", position));
        }
    }

    /**
     * Method to add new element to begining of linked list.
     *
     * @param element new element to be added
     * @return true if added successfully.
     */
    final public boolean addFirst(P element) {
        final Node<P> head = this.first;
        final Node<P> newElement = new Node<>(element, null, head);

        this.first = newElement;
        if (head == null) {
            this.last = newElement;
        } else {
            head.prev = newElement;
        }
        try {
            this.increaseQueueSize(1);
            return true;
        } catch (DataFormatException e) {
            return false;
        }
    }

    /**
     * Method to add new element before a specified element.
     *
     * @param element  new element data.
     * @param follower node to follow newly created element
     * @return true if element was successfully added to list.
     */
    final public boolean addBefore(P element, Node<P> follower) {
        final Node<P> prev = follower.prev;
        final Node<P> newElement = new Node<>(element, prev, follower);

        follower.prev = newElement;

        return insertNewElement(prev, newElement);
    }

    /**
     * Method to insertElement.
     * Connected to add methods. Once new element has been created, determines where to place it.
     *
     * @param prev       previous element to newElement
     * @param newElement is the newly created element
     * @return true when newElement has been added successfully.
     */
    private boolean insertNewElement(Node<P> prev, Node<P> newElement) {
        if (prev == null) {
            this.first = newElement;
        } else {
            prev.next = newElement;
        }
        try {
            this.increaseQueueSize(1);
            return true;
        } catch (DataFormatException e) {
            return false;
        }
    }

    /**
     * Method to add new element to the end of the list.
     *
     * @param element new element to be added.
     * @return true if element was added successfully.
     */
    final public boolean addLast(P element) {
        final Node<P> tail = this.last;
        final Node<P> newElement = new Node<>(element, tail, null);

        this.last = newElement;

        return insertNewElement(tail, newElement);
    }

    /**
     * Method to get first element in list.
     *
     * @return first element of list given that it exists.
     * @throws NoSuchElementException should list not have any elements.
     **/
    final public P getFirst() {
        final Node<P> head = this.first;
        if (head == null) {
            throw new NoSuchElementException("Element does exist at head");
        }
        return head.move;
    }

    /**
     * Method to get last element on list.
     *
     * @return last element of list given that it exists.
     * @throws NoSuchElementException if element does not exist.
     **/
    final public P getLast() {
        final Node<P> tail = this.last;

        if (tail == null) {
            throw new NoSuchElementException("Last Element Not Present");
        }
        return tail.move;
    }

    /**
     * Method to get the first index of an element
     *
     * @param element item searching for
     * @return index or position of item. IF -1 is returned item not found.
     */
    @Override
    final public int indexOf(Object element) {
        int index = 0;

        if (element == null) {
            for (Node<P> i = this.first; i != null; i = i.next) {
                if (i.move == null) {
                    return index;
                }
                index += 1;
            }
        } else {
            for (Node<P> i = this.first; i != null; i = i.next) {
                if (element.equals(i.move)) {
                    return index;
                }
                index += 1;
            }
        }
        return -1;
    }

    /**
     * Method to find last index of a particular element.
     *
     * @param element item to search for.
     * @return location of element as an int. IF not found result is -1.
     */
    @Override
    final public int lastIndexOf(P element) {
        int index = 0;
        Node<P> i;

        if (element == null) {
            for (i = this.last; i != null; i = i.prev) {
                index -= 1;

                if (i.move == null) {
                    return index;
                }
            }
        } else {
            for (i = this.last; i != null; i = i.prev) {
                index -= 1;
                if (element.equals(i.move)) {
                    return index;
                }
            }
        }
        return -1;
    }

    /**
     * Method to iterate throughout list
     *
     * @param index where iterator should start
     * @return PlayIterator instance.
     */
    @Override
    final public ListIterator listIterator(int index) {
        this.isValidPosition(index);

        return new PlayIterator(index);
    }

    /**
     * Method to remove element by position or index;
     *
     * @param position or index to remove
     * @return element removed;
     */
    @Override
    public P remove(int position) {
        isValidPosition(position);
        return withdrawElement(getNode(position));
    }

    /**
     * Method to remove first occurrence of element from list.
     *
     * @param element to be removed.
     * @return true if element was successfully removed. Else returns false if withdraw method failed or
     * false if itself failed.
     */
    @Override
    final public boolean remove(Object element) {
        Node<P> i;
        if (element == null) {
            for (i = this.first; i != null; i = i.next) {
                if (i.move == null) {
                    return this.withdrawElement(i) != null;
                }
            }
        } else {
            for (i = this.first; i != null; i = i.next) {
                if (element.equals(i.move)) {
                    return this.withdrawElement(i) != null;
                }
            }
        }
        return false;
    }

    /**
     * Method to do all the heavy lifting in pulling element from list.
     * Solves my prior problem of having null still in the list.
     *
     * @param element Node to be removed;
     * @return Node.move of withdrawn element or null if failed.
     */
    private P withdrawElement(Node<P> element) {
        final P item = element.move;
        final Node<P> next = element.next;
        final Node<P> previous = element.prev;

        if (previous == null) {
            this.first = next;
        } else {
            previous.next = next;
            element.prev = null;
        }

        if (next == null) {
            this.last = previous;
        } else {
            next.prev = previous;
            element.next = null;
        }

        element.move = null;
        try {
            this.decreaseQueueSize(1);
            return item;
        } catch (DataFormatException e) {
            return null;
        }
    }

    /**
     * similar to withdrawElement but just withdraws the first node
     *
     * @param element Node to be removed.
     * @return element that was removed or null if unsuccessful
     */
    private P withdrawFirstElement(Node<P> element) {
        final P item = element.move;
        final Node<P> nextItem = element.next;

        element.move = null;
        element.next = null;

        this.first = nextItem;
        if (nextItem == null) {
            this.last = null;
        } else {
            nextItem.prev = null;
        }
        try {
            this.decreaseQueueSize(1);
            return item;
        } catch (DataFormatException e) {
            return null;
        }
    }

    /**
     * similar to withdrawElement but just withdraws the last node
     *
     * @param element Node to be removed.
     * @return element that was removed or null if unsuccessful
     */
    private P withdrawLastElement(Node<P> element) {
        final P item = element.move;
        final Node<P> previous = element.prev;

        element.move = null;
        element.prev = null;
        this.last = previous;
        if (previous == null) {
            this.first = null;
        } else {
            previous.next = null;
        }
        try {
            this.decreaseQueueSize(1);
            return item;
        } catch (DataFormatException e) {
            return null;
        }
    }

    /**
     * Method to set value of Node at a particular position
     *
     * @param position at which to set.
     * @param element  at which to change
     * @return changed Element
     */
    @Override
    final public P set(int position, P element) {
        if (isValidPosition(position)) {
            Node<P> node = getNode(position);
            P changeValue = node.move;
            node.move = element;
            return changeValue;
        }
        return null;
    }

    /**
     * Determines if list has any elements.
     **/
    @Override
    final public boolean isEmpty() {
        return (this.queueSize < 0) && (this.first != null);
    }

    /**
     * Clear History List
     **/
    @Override
    final public void clear() {
        this.first = null;
        this.last = null;
        this.queueSize = 0;
    }

    /**
     * Method to get element of node.
     *
     * @param position or index of node.
     * @return element of specified Node.
     */
    @Override
    final public P get(int position) {
        isValidPosition(position);
        return getNode(position).move;
    }

    /**
     * Method to get full node by index or position
     *
     * @param position or index
     * @return Node at position.
     */
    private Node<P> getNode(int position) {
        if (position < this.queueSize) {
            Node<P> head = this.first;

            for (int i = 0; i < position; i++) {
                head = head.next;
            }
            return head;
        } else {
            Node<P> head = this.last;
            for (int i = this.queueSize - 1; i > position; i--) {
                head = head.next;
            }
            return head;
        }
    }


    /**
     * Remove First Element of list
     *
     * @return first element from list.
     * @throws NoSuchElementException if there are no elements in the list
     **/
    final public P removeFirst() {
        final Node<P> head = this.first;
        if (head == null) {
            throw new NoSuchElementException("Element Does Not Exist");
        }
        return this.withdrawFirstElement(head);
    }

    /**
     * Removes last element on list.
     *
     * @return last element that was removed.
     * @throws NoSuchElementException if list was empty.
     */
    public P removeLast() {
        final Node<P> tail = this.last;
        if (tail == null) {
            throw new NoSuchElementException("No Element found at tail of list");
        }
        return this.withdrawLastElement(tail);
    }

    /**
     * Overrides method in interface to determine if list contains element
     *
     * @param element to search for.
     * @return true if list contains element.
     */
    @Override
    final public boolean contains(Object element) {
        return this.indexOf(element) >= 0;
    }

    /**
     * At this point I do not understand what the difference between this and the listIterator are.
     *
     * @return Iterator
     */
    @Override
    public Iterator<P> iterator() {
        return new PlayIterator(0);
    }


    /**
     * Output list to an array. Overrides interface method.
     *
     * @return Array of current list items.
     */
    @Override
    public Object[] toArray() {
        Object[] newArray = new Node[this.queueSize];
        int i = 0;
        for (Node<P> n = this.first; n != null; n = n.next) {
            newArray[i++] = n.move;
        }
        return newArray;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends P> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");

        Node<P> current = this.first;
        for (int i = 0; i < queueSize; i++) {
            result.append(current.move);
            current = current.next;
            if (current != null) {
                result.append(", ");
            } else {
                result.append("]");
            }
        }
        return result.toString();
    }

    private static class Node<P> {
        P move;
        Node<P> next;
        Node<P> prev;

        public Node(P move, Node<P> previousNode, Node<P> nextNode) {
            this.move = move;
            this.next = nextNode;
            this.prev = previousNode;
        }

        @Override
        final public boolean equals(Object move) {
            return move.toString() == this.move.toString();
        }

        @Override
        public String toString() {
            return this.move.toString();
        }
    }

    private class PlayIterator implements ListIterator<P> {
        int futurePosition;
        private Node<P> current;
        private Node<P> future;

        /**
         * Constructor for our list iterator
         *
         * @param position or index of starting point/
         */
        public PlayIterator(int position) {
            this.future = (position == queueSize) ? null : getNode(position);
            futurePosition = position;
        }

        public PlayIterator(Node<P> node, int index) {
            this.current = node;
            this.futurePosition = index;
        }

        /**
         * Method to determine if iterator has a next Node.
         *
         * @return true if list has more items.
         */
        @Override
        final public boolean hasNext() {
            return futurePosition < queueSize;
        }

        /**
         * Method to move to the next item in the iterator.
         *
         * @return next item on list.
         */
        @Override
        final public P next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            current = future;
            future = future.next;
            futurePosition += 1;
            return current.move;
        }

        /**
         * Method to see if there are items to go back to.
         *
         * @return true if previous items exist.
         */
        @Override
        final public boolean hasPrevious() {
            return futurePosition > 0;
        }

        /**
         * Method to get previous item on list.
         *
         * @return previous element.
         */
        @Override
        final public P previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }

            current = future = (future == null) ? last : future.prev;

            futurePosition -= 1;
            return current.move;
        }

        /**
         * Method to obtain the index of the next item.
         *
         * @return int index of next item.
         */
        @Override
        final public int nextIndex() {
            return this.futurePosition;
        }

        /**
         * Method to obtain the previous index.
         *
         * @return futurePosition - 1
         */
        @Override
        final public int previousIndex() {
            return this.futurePosition - 1;
        }

        /**
         * Implementation of remove method found in ListIterator interface.
         */
        @Override
        final public void remove() {
            if (current == null) {
                throw new IllegalStateException("Can't Remove A Null Element");
            }

            Node<P> currentNext = current.next;
            withdrawElement(currentNext);
            if (future == current) {
                future = currentNext;
            } else {
                futurePosition -= 1;
            }
            current = null;
        }

        /**
         * Overriding Method in interface to change current Node.
         *
         * @param element to
         */
        @Override
        public void set(P element) {
            if (current == null) {
                throw new IllegalStateException("Current should not be null. An error occurred");
            }
            current.move = element;

        }

        /**
         * Override method in interface to add to list
         *
         * @param element to be added.
         */
        @Override
        final public void add(P element) {
            current = null;
            if (future == null) {
                addLast(element);
            } else {
                addBefore(element, future);
            }
            this.futurePosition += 1;
        }
    }
}
