import GameQueue.MyPlayHistory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test for LinkedList Implementation and Slider Puzzle.
 * Coded for Assignment 1 of CS 3 Spring 2019.
 *
 * @author Christopher J Bigelow, A01771452
 */
class SliderPuzzleTest {
    String a = "Up";
    String b = "Down";
    String c = "Left";
    String d = "Right";

    @Test
    public void showMeBoard() {
        Board myBoard = new Board();
        myBoard.makeBoard(3);
        myBoard.showMe(myBoard, "LDRUL");
    }

    @Test
    public void cloneBoard() {
        Board goalBoard = new Board();
        int[] values = {
                1, 2, 3,
                4, 5, 6,
                7, 8, 0
        };
        goalBoard.makeBoard(values);

        Board cloneGoal = goalBoard.duplicate();
        cloneGoal.slideRight();
        System.out.printf("Goal Board is:\n%s\n", goalBoard);
        System.out.printf("Cloneboard is:\n%s\n", cloneGoal);

        int[] target = {
                1, 2, 3,
                4, 5, 6,
                7, 0, 8
        };

        Board targetBoard = new Board();
        targetBoard.makeBoard(target);
        System.out.printf("Targetboard is:\n%s\n", targetBoard);
        Assertions.assertFalse(goalBoard.equals(cloneGoal));
        Assertions.assertTrue(targetBoard.equals(cloneGoal));

    }
    @Test
    public void constructorTest() {
        MyPlayHistory<String> myList1 = new MyPlayHistory<>();
        Assertions.assertEquals(0, myList1.size());
    }

    @Test
    public void constructorWithNode() {
        MyPlayHistory<String> myList1 = new MyPlayHistory<>(a);
        Assertions.assertEquals(1, myList1.size());
    }

    @Test
    public void setupAsArray() {
        MyPlayHistory<String> moves = new MyPlayHistory<>();


        String[] myMoves = {a, b, c, d};
        MyPlayHistory<String> moves1 = new MyPlayHistory<>(myMoves);

        boolean wereAdded = moves.addAll(myMoves);

        Assertions.assertTrue(wereAdded, String.format("Array was added correctly %b", wereAdded));
        Assertions.assertEquals(4, moves1.size());
    }

    @Test
    public void asInividuals() {
        MyPlayHistory<String> myList = new MyPlayHistory<>();
        addToList(myList, a, b);
        addToList(myList, c, d);
    }

    @Test
    public void getRightIndex() {
        MyPlayHistory<String> myList = new MyPlayHistory<>();
        addToList(myList, a, b);
        addToList(myList, c, d);

        int index = myList.indexOf(a);
        Assertions.assertEquals(0, index);
        int indexB = myList.indexOf(b);
        Assertions.assertEquals(1, indexB);

    }

    @Test
    public void getCorrectSize() {
        MyPlayHistory<String> myList = new MyPlayHistory<>();
        addToList(myList, a, b);
        addToList(myList, c, d);

        Assertions.assertEquals(4, myList.size());
    }

    @Test
    public void addAtPoint() {

        MyPlayHistory<String> myList = new MyPlayHistory<>();
        addToList(myList, a, b);
        addToList(myList, c, d);
        String e = "Around";

        boolean wasAdded = myList.add(3, e);

        String indexThree = myList.get(3);

        Assertions.assertEquals(indexThree, "Around");

        Assertions.assertTrue(wasAdded);
        Assertions.assertEquals(5, myList.size());


    }

    private void addToList(MyPlayHistory<String> mylist, String c, String d) {
        boolean didCAdd = mylist.add(c);
        Assertions.assertTrue(didCAdd, String.format("A was added %b", didCAdd));
        boolean didDAdd = mylist.add(d);
        Assertions.assertTrue(didDAdd, String.format("A was added %b", didDAdd));
    }
}