# Assignment 1: Slider Board
## CS 3 - Data Structures and Algorithms.
### Due 21 January 2019 at 11:59pm MST.

Files Provided By Professor: `Board.java` & `Game.java`
README adapted from assignment specifications provided by professor.

Coded By Christopher Bigelow, Utah State University.

**The problem**. The slider puzzle is played on a 3-by-3 grid with 9 square blocks labeled 1 through 8 (with one blank). The goal is to rearrange the blocks so that they are in order. Blocks can be slide horizontally or vertically into the blank spot.

#### Part 1 - Making Moves
Write the “ShowMe” method which takes a board and a sequence of moves and shows the boards that result from those moves.  The moves are all relative to the blank square.

#### Part 2 - Solving the Problem
Write a program to SOLVE the puzzle. **From a specific board, illustrate an efficient solution.**

#### The Intelligence
Instead of doing one move after another – consider all boards reachable with one move, then all boards reachable with two moves, etc. While some original games have four “next games”, other games have fewer (due to the position of the blank and because we don’t want to undo the previous move).  This is like traversing the logical game tree “by level”.  This is a called a breadth-first traversal. Examine all one move states, then all two move states, etc.

 